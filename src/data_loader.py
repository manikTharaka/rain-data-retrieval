import os
import csv


def read_csv(file_path,max_lines=10e10):
    csv_file = open(file_path)                   
    csv_reader = csv.reader(csv_file, delimiter=",")

    line_count = 0
    rain_data = list()
    for row in csv_reader:
        line_count += 1
        if line_count <= 2:
            continue
        elif line_count >= max_lines:
            break
        rain_data.append(row)
    
    csv_file.close()

    return rain_data


def get_osm_url(city_name):
    return f"https://nominatim.openstreetmap.org/search.php?city={city_name}&format=jsonv2&namedetails=0&addressdetails=0&limit=1"

