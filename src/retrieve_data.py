import requests


from data_loader import read_csv, get_osm_url

# rain data from .
# https://coastwatch.pfeg.noaa.gov/erddap/griddap/chirps20GlobalPentadP05.html
# used dataset:
# https://coastwatch.pfeg.noaa.gov/erddap/griddap/chirps20GlobalPentadP05.csv?precip%5B(2021-8-01T00:00:00Z):1:(2021-11-26T00:00:00Z)%5D%5B(30.0):.25:(42.0)%5D%5B(-123.0):.25:(-113.0)%5D


# The url used for the data:
# https://coastwatch.pfeg.noaa.gov/erddap/griddap/chirps20GlobalPentadP05.csv?precip%5B(2021-8-01T00:00:00Z):1:(2021-11-26T00:00:00Z)%5D%5B(30.0):.5:(42.0)%5D%5B(-123.0):.5:(-113.0)%5D
# a stride of 0.5 was used as 0.25 gave an error.
file_name = "data/chirps20GlobalPentadP05_233e_7ccc_b137.csv"


def get_rainy_days(rain_data, city_lat, city_lon):

    dist_thresh = 0.05
    rain_thresh = 8.0

    dates = list()
    for row in rain_data:
        t, lat, lon, rain = row
        t = t[:10]
        lat_diff = abs(float(lat) - float(city_lat))
        lon_diff = abs(float(lon) - float(city_lon))
        if rain != "NaN":
            if float(rain) >= rain_thresh:
                if lat_diff < dist_thresh:
                    if lon_diff < dist_thresh:
                        dates.append((t, rain))

    return dates


if __name__ == "__main__":
    city_name = str(input("Enter city name:[San Jose]") or "San Jose")

    rain_data = read_csv(file_name)

    response = requests.get(get_osm_url(city_name))

    city_data = response.json()

    if len(city_data) == 0:
        raise ValueError(
            "Could not find coordinates data for the specified city. Please try antoher city name"
        )

    c_lat = city_data[0]["lat"]
    c_lon = city_data[0]["lon"]

    dates = get_rainy_days(rain_data, c_lat, c_lon)

    for item in dates:
        print(item)
    print("number of rainy 5-days: " + str(len(dates)))
