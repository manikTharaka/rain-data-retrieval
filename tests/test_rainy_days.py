from multiprocessing.sharedctypes import Value
import sys
import pytest

sys.path.insert(1, "src")

from retrieve_data import get_rainy_days
from data_loader import read_csv, get_osm_url


file_name = "data/chirps20GlobalPentadP05_233e_7ccc_b137.csv"


def test_get_rainy_days():

    rain_data = [
        ["2021-08-06T00:00:00Z", "38.274994", "-116.27501", "0.88126415"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.225006", "0.89729786"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.175", "10.99447185"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.125", "9.0284044"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.075005", "1.1305686"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.02501", "1.1945685"],
    ]

    lat = "38.274994"
    lon = "-116.125"

    expected_days = [
        ("2021-08-06", "10.99447185"),
        ("2021-08-06", "9.0284044"),
    ]

    result = get_rainy_days(rain_data, lat, lon)

    assert expected_days == result


def test_below_rain_thresh():
    rain_data = [
        ["2021-08-06T00:00:00Z", "38.274994", "-116.27501", "0.88126415"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.225006", "0.89729786"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.175", "7.99447185"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.125", "6.0"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.075005", "1.1305686"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.02501", "1.1945685"],
    ]

    lat = "38.274994"
    lon = "-116.125"

    result = get_rainy_days(rain_data,lat,lon)

    assert [] == result


def  test_below_dist_thresh():
    
    rain_data = [
        ["2021-08-06T00:00:00Z", "38.274994", "-116.27501", "0.88126415"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.225006", "0.89729786"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.180", "10.99447185"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.190", "9.0284044"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.075005", "1.1305686"],
        ["2021-08-06T00:00:00Z", "38.274994", "-116.02501", "1.1945685"],
    ]

    lat = "38.274994"
    lon = "-116.125"

    result = get_rainy_days(rain_data,lat,lon)

    assert [] == result

def test_read_csv_wrong_filename():
    with pytest.raises(FileNotFoundError):
        read_csv("invalid/file/path")


def test_read_csv_not_None():
    assert read_csv(file_name) is not None


def test_read_csv_max_lines():
    assert len(read_csv(file_name)) <= 10e10


def test_read_csv_skip_header():
    csv = read_csv(file_name)

    assert csv[0][0][:4].isnumeric()


def test_get_openstreet_url_san_jose():
    expected = "https://nominatim.openstreetmap.org/search.php?city=San Jose&format=jsonv2&namedetails=0&addressdetails=0&limit=1"

    assert get_osm_url("San Jose") == expected


def test_get_openstreet_url_san_diego():
    expected = "https://nominatim.openstreetmap.org/search.php?city=San Diego&format=jsonv2&namedetails=0&addressdetails=0&limit=1"

    assert get_osm_url("San Diego") == expected
